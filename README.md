# python-sharepoint-upload
Upload CR to Sharepoint

## Requirements
- python 3
- requests
- lxml

## Usage
```console
python3 src/main.py -f "2016_11_10_CA_CR.pdf" "2016_11_09_BUREAU_CR.pdf"
```

The script will autodetect the target team (only CA or BUREAU) and upload the file to the right place.

## Integration
This script is easily integrable with the Finder in macOS :
- Create an Automator Service
- "PDF file" in input
- Run AppleScript with :

```applescript
on run {input, parameters}
	if application "iTerm" is running or application "iTerm" is running then
		run script "
			on run {input, parameters}
				tell application \":Applications:iTerm.app\"
					activate
					set filesString to \"\"
					repeat with file_ in input
						set filesString to filesString & \" \" & quoted form of (POSIX path of file_)
					end repeat
					try
						select first window
						set onlywindow to false
					on error
						create window with default profile
						select first window
						set onlywindow to true
					end try
					tell the first window
						if onlywindow is false then
							create tab with default profile
						end if
						tell current session to write text \"/usr/local/bin/python3 <path-to-project>/src/main.py -f \" & filesString
					end tell
				end tell
			end run
		" with parameters {input, parameters}
	else
		run script "
			on run {input, parameters}
				tell application \":Applications:iTerm.app\"
					activate
					set filesString to \"\"
					repeat with file_ in input
						set filesString to filesString & \" \" & quoted form of (POSIX path of file_)
					end repeat
					activate
					try
						select first window
					on error
						create window with default profile
						select first window
					end try
					tell the first window
						tell current session to write text \"/usr/local/bin/python3 <path-to-project>/src/main.py -f \" & filesString
					end tell
				end tell
			end run
		" with parameters {input, parameters}
	end if
end run
```

This script will open iTerm or use an existing instance and run the script with the selected files in the Finder.


## Licence

[![GNU GPL v3.0](http://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl.html)

```
Sharepoint CR Upload - Upload CR to Sharepoint according to their belonging team and the CR date.
Copyright (C) 2016 Gabriel AUGENDRE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
