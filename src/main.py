# coding: utf-8

import argparse
import os
import urllib.parse
from getpass import getpass

import grequests
import requests
import sys
from lxml import etree

try:
    import config
except ImportError:
    config = None

SERVER_URL = "https://bdeinsalyon.sharepoint.com"

LOGIN_URL = SERVER_URL + "/_forms/default.aspx?wa=wsignin1.0"
REQUEST_DIGEST_URL = SERVER_URL + "/_api/contextinfo"

SECURITY_TOKEN_URL = "https://login.microsoftonline.com/extSTS.srf"


def get_credentials():
    if config is not None:
        username = config.USERNAME
        password = config.PASSWORD
    else:
        username = input("Username ? ")
        password = getpass("Password ? ")

    return username, password


def sec_token_payload(username, password, endpoint):
    return """
    <s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"
          xmlns:a="http://www.w3.org/2005/08/addressing"
          xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
      <s:Header>
        <a:Action s:mustUnderstand="1">http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue</a:Action>
        <a:ReplyTo>
          <a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>
        </a:ReplyTo>
        <a:To s:mustUnderstand="1">https://login.microsoftonline.com/extSTS.srf</a:To>
        <o:Security s:mustUnderstand="1"
           xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
          <o:UsernameToken>
            <o:Username>{username}</o:Username>
            <o:Password>{password}</o:Password>
          </o:UsernameToken>
        </o:Security>
      </s:Header>
      <s:Body>
        <t:RequestSecurityToken xmlns:t="http://schemas.xmlsoap.org/ws/2005/02/trust">
          <wsp:AppliesTo xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy">
            <a:EndpointReference>
              <a:Address>{endpoint}</a:Address>
            </a:EndpointReference>
          </wsp:AppliesTo>
          <t:KeyType>http://schemas.xmlsoap.org/ws/2005/05/identity/NoProofKey</t:KeyType>
          <t:RequestType>http://schemas.xmlsoap.org/ws/2005/02/trust/Issue</t:RequestType>
          <t:TokenType>urn:oasis:names:tc:SAML:1.0:assertion</t:TokenType>
        </t:RequestSecurityToken>
      </s:Body>
    </s:Envelope>
    """.format(username=username, password=password, endpoint=endpoint)


def xml_from_soap(soap):
    return etree.fromstring(soap.text.split('?>', maxsplit=1)[1])


def extract_security_token(response):
    xml = xml_from_soap(response)
    return xml.find('.//{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd}BinarySecurityToken').text


def extract_digest(response):
    xml = xml_from_soap(response)
    return xml.find('.//{http://schemas.microsoft.com/ado/2007/08/dataservices}FormDigestValue').text


def extract_final_url(response):
    xml = xml_from_soap(response)
    return xml.find('.//{http://schemas.microsoft.com/ado/2007/08/dataservices}ServerRelativeUrl').text


def is_ca(filename):
    return 'ca' in filename.lower()


def main(user_files):
    username, password = get_credentials()
    payload = sec_token_payload(username, password, SERVER_URL)

    print('Authentification en cours...')

    cookies = {}
    digest = ''

    try:
        sec_token = extract_security_token(requests.post(SECURITY_TOKEN_URL, data=payload))
        cookies = requests.post(LOGIN_URL, data=sec_token).cookies
        digest = extract_digest(requests.post(REQUEST_DIGEST_URL, cookies=cookies))

    except (AttributeError, IndexError):
        print("Erreur lors de l'authentification...")
        exit(1)

    else:
        print('Authentification réussie !')

    headers = {
        'Accept': 'application/xml;odata=verbose',
        'Authorization': 'Bearer ' + digest,
    }

    success = []
    reqs = []
    opened_files = [open(user_file, mode='rb') for user_file in user_files]

    for f in opened_files:
        file_name = os.path.basename(f.name)
        ca = is_ca(file_name)

        cr_url = SERVER_URL + "/teams/{team}/_api/web/getfolderbyserverrelativeurl('/teams/{team}/{path}')/files"

        if ca:
            meeting_date = file_name.split('_', maxsplit=4)
            del(meeting_date[4])
            meeting_date = '_'.join(meeting_date)

            team = 'ca'
            path = 'Documents partages/Réunions CA/2017-2018/{meeting}'.format(meeting=meeting_date)

        else:
            team = 'bureau'
            path = 'Documents partages/REUNIONS BUREAU/2017-2018'

        cr_url = cr_url.format(team=team, path=path)

        add_cr_url = cr_url + "/add(overwrite=true,url='{filename}')".format(filename=file_name)

        print('Envoi de {0} en cours...'.format(file_name))
        reqs.append(grequests.post(add_cr_url, cookies=cookies, headers=headers, data=f))

    res = grequests.map(reqs, exception_handler=lambda req, ex: print('Erreur', req, ex))
    for add_cr_res in res:
        try:
            final_url = SERVER_URL + urllib.parse.quote(extract_final_url(add_cr_res))
        except AttributeError:
            print("Ereur lors de l'envoi du fichier...", file=sys.stderr)
        else:
            print('Success : ' + final_url)
            success.append(final_url)
    for f in opened_files:
        f.close()

    print('\nRécapitulatif :')
    print(' '.join(success))


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-f', '--files', type=str, nargs='+', help='Files to upload.', required=True)

    args = ap.parse_args()
    files = args.files

    main(files)
